//#define NDEBUG
//#define SHAPE_CHECK

#include <iostream>
#include "Tensor.hpp"
#include "Clock.hpp"

Clock c;

void init_data(Tensor<double> & x, Tensor<double> & y, Tensor<double> & z) {
  unsigned long k;
  for (k=0; k<x.flat_size(); ++k)
    x[k] = k / double(k+1);
  for (k=0; k<y.flat_size(); ++k)
    y[k] = k*(k-1);
  for (k=0; k<z.flat_size(); ++k)
    z[k] = k+3;
}

int main() {
  Tensor<double> x({(1<<7)+1, 1<<5, (1<<3)+5, 1<<2, 1<<5});
  Tensor<double> y({(1<<7)+1, 1<<5, (1<<3)+5, 1<<2, 1<<5});
  Tensor<double> z({(1<<7)+1, 1<<5, (1<<3)+5, 1<<2, 1<<5});

  unsigned int DIMENSION = 5;
  init_data(x,y,z);

  std::cout << "computing inner product <x,y>\n";
  c.tick();
  double tot = 0.0;
  for_each_tensors([&tot](double xV, double yV) {
      tot += xV * yV;
    },
    x.data_shape(),
    x, y);
  c.ptock();
  std::cout << "tot " << tot << std::endl;
  std::cout << std::endl;

  std::cout << "performing x *= y + z (naive)\n";
  c.tick();
  x.flat() *= y.flat() + z.flat();
  c.ptock();
  std::cout << std::endl;

  std::cout << "performing x *= y + z\n";
  c.tick();
  auto x_view = x.start_at({1,1,1,1,1});
  apply_tensors([](double & xV, double yV, double zV) {
      xV *= yV + zV;
    },
    x_view.view_shape(),
    x_view, y, z);
  c.ptock();
  std::cout << std::endl;

  std::cout << "printing Tensor and TensorView:\n";
  Tensor<int> ten({2,3,4});
  for (unsigned long k=0; k<ten.flat_size(); ++k)
    ten[k] = k;
  std::cout << ten << std::endl;
  std::cout << ten.start_at({1,1,1}) << std::endl;
  std::cout << std::endl;

  std::cout << "computing sum_t t * x[t] * y[t] * z[t]\n";
  c.tick();
  Vector<double> result(DIMENSION);
  result.fill(0.0);
  enumerate_for_each_tensors([&result](const_tup_t counter, const unsigned int dim, double xV, double yV, double zV) {
      for (unsigned int k=0; k<dim; ++k)
	result[k] += counter[k] * xV * yV * zV;
    },
    bounding_shape( x_view, y, z ),
    x_view, y, z);
  c.ptock();
  std::cout << "result " << result << std::endl;
  std::cout << std::endl;
}
